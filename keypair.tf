resource "aws_key_pair" "default_keypair" {
  key_name = "MyEC2KeyPair"
  public_key = "ssh-rsa xxx"
}