# ---------------------------------------------------------------------------------------------------------------------
# ENVIRONMENT VARIABLES
# Define these secrets as environment variables
# ---------------------------------------------------------------------------------------------------------------------

# AWS_ACCESS_KEY_ID
# AWS_SECRET_ACCESS_KEY

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# ---------------------------------------------------------------------------------------------------------------------

variable "server_port" {
  description = "The port the server will use for HTTP requests"
  default = 80
}

#variable "security_group_id" {
#    description = "Security Group id"
#}

variable "db_user" {
  description = "The user for the database"
  default = "root"
}
variable "db_password" {
  description = "The password for the database"
  default = "12345678"
}
#data "template" "user_data" {
#  template = "${file("wp-fault-tolerant/templates/user_data.tpl")}"
#}
variable "ssh_key" {
  default = "MyEC2KeyPair"
}

variable "INSTANCE_USERNAME" {
default = "ec2-user"
}

variable "PATH_TO_PRIVATE_KEY" {
default = "MyEC2KeyPair.pem"
}
variable "PATH_TO_PUBLIC_KEY" {
default = "MyEC2KeyPair.pub"
}