# ------------------------------------------------------------------------------
# CONFIGURE OUR AWS CONNECTION
# ------------------------------------------------------------------------------

provider "aws" {
  region = "us-east-1" # US EAST - N.Virginia
  #region = "eu-central-1" # EU - Frankfurt

#specify credentials from file  
  shared_credentials_file = "credentials.txt"
  profile = "terraform"
}
