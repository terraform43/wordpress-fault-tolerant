# ---------------------------------------------------------------------------------------------------------------------
# CREATE AN ELB TO ROUTE TRAFFIC ACROSS THE AUTO SCALING GROUP
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_elb" "ELB-test" {
  name = "ELB-test"
  security_groups = ["${aws_security_group.WEB-DMZ.id}"]
  availability_zones = ["${data.aws_availability_zones.all.names}"]

  # This adds a listener for incoming HTTP requests.
  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "${var.server_port}"
    instance_protocol = "http"
  }
  
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 30
    #target = "HTTP:${var.server_port}/"
	target = "TCP:22"
  }
}