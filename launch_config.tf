# ---------------------------------------------------------------------------------------------------------------------
# CREATE A LAUNCH CONFIGURATION THAT DEFINES EACH EC2 INSTANCE IN THE ASG
# ---------------------------------------------------------------------------------------------------------------------
# The aws_launch_configuration resource uses almost	exactly	the	same parameters
# as the aws_instance resource, so you can replace the latter with the former pretty easily

resource "aws_launch_configuration" "ASG-launch-config" {
  #name = "ASG-launch-config"  # see: https://github.com/hashicorp/terraform/issues/3665
  name_prefix = "ASG-launch-config-"
  image_id = "ami-a4dc46db" #Ubuntu 16.04 LTS
  #image_id = "ami-b70554c8" #Amazon Linux 2
  instance_type = "t2.micro"
  security_groups = ["${aws_security_group.WEB-DMZ.id}"]
  key_name = "MyEC2KeyPair"
  #user_data = <<-EOF
  #            #!/bin/bash
  #            echo "Hello, World" > index.html
  #            nohup busybox httpd -f -p "${var.server_port}" &
  #          EOF


  user_data = <<HEREDOC
  #!/bin/bash
  # sleep until instance is ready
  until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
    sleep 1
  done
  # install nginx
  apt-get update
  apt-get -y install nginx
  # make sure nginx is started
  service nginx start 
  HEREDOC

  lifecycle {
    create_before_destroy = true
  }
}